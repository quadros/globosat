from django.db import models
from django.utils import timezone
from datetime import date

class Evento(models.Model):
    evento_calendario = models.DateField(default=date.today)
    evento_lutador_1 = models.CharField(max_length=200, default="Nome do primeiro lutador")
    evento_lutador_2 = models.CharField(max_length=200, default="Nome do segundo lutador")
    evento_subtitulo = models.CharField(max_length=200, default='Subtítulo do evento')
    evento_img_arquivo = models.FileField(upload_to='img/eventos', null=True)
    def __str__(self):
        return ('Evento: %s X %s' % (self.evento_lutador_1, self.evento_lutador_2))

