from django.http import HttpResponse
from django.template import loader

from .models import Evento

def index(request):
    eventos_lista = Evento.objects.all().order_by('evento_calendario')
    template = loader.get_template('eventos/index.html')
    context = {
        'eventos_lista': eventos_lista,
    }
    return HttpResponse(template.render(context, request))